/*
 * @author: Michal Paulovic (xpaulovicm1)
 *
 * CMAKE:
 *      CMAKE_C_STANDARD 99
 *      CMAKE_MINIMUM_REQUIRED VERSION 3.17
 *
 * GCC_VERSION 10.2.0
 **/

#ifndef MEMALLOC
    #define MEMALLOC

    /* @source: https://stackoverflow.com/questions/3219393/stdlib-and-colored-output-in-c */
    #define ANSI_RED     "\x1b[31m"
    #define ANSI_GREEN   "\x1b[32m"
    #define ANSI_YELLOW  "\x1b[33m"
    #define ANSI_BLUE    "\x1b[34m"
    #define ANSI_RST     "\x1b[0m"

    struct header {
        unsigned short offset_of_next;
        short size;
    };

    extern void *MEMORY;

    /* ----- Debug and test features ----- */

    void debug_print(void *, unsigned int);

    void debug_print_list(void *);

    /* ------------ Utilities ------------ */

    struct header *get_header(unsigned int);

    int get_offset(struct header *);

    void merge_free_blocks(unsigned int , unsigned int);

    /* ------------ Assigment ------------ */

    void memory_init(void *, unsigned int);

    int memory_check(void *);

    void *memory_alloc(unsigned int);

    int memory_free(void *);

#endif