/* @author: Michal Paulovic (xpaulovicm1)
 *
 * C Coding Standard
 *      @source: https://users.ece.cmu.edu/~eno/coding/CCodingStandard.html
 *
 * CMAKE:
 *      CMAKE_C_STANDARD 99
 *      CMAKE_MINIMUM_REQUIRED VERSION 3.17
 *
 * GCC:
 *      GCC_VERSION 10.2.0
 *
 * Usage example with GCC:
 * $ cd src/
 * $ gcc 10_test.c ../src/memalloc.c -o ../bin/10_test
 * $ ./../bin/memloc_bin
 *
 * */

#include <stdio.h>
#include <string.h>
#include "memalloc.h"

#define BYTE unsigned char
#define HEADER_SIZE sizeof(struct header)
#define SMALLEST_BLOCK (sizeof(BYTE) + HEADER_SIZE)

/* Declaration of boolean type */
typedef enum { FALSE, TRUE } boolean;

/* Pointer to memory block */
void *MEMORY;

/* ----- Debug and test features ----- */

void debug_print(void *ptr, unsigned int size) {
    char *mem = ptr;

    printf(ANSI_GREEN "Overview:" ANSI_RST "\n");
    for(int i = 0; i < size; i++){
        if (i % sizeof(typeof(size)) == 0 && i != 0) printf("| ");

        if (i % 16 == 0 && i != 0) printf("\n");

        /* Body of allocated block */
        if (mem[i] == 5){
            printf(ANSI_BLUE "%d " ANSI_RST, mem[i]);
        }
        /* First byte of allocated block (if not last) */
        else if (mem[i] == 2){
            printf(ANSI_GREEN "%d " ANSI_RST, mem[i]);
        }
        /* Hanging bytes */
        else if (mem[i] == 3) {
            printf(ANSI_YELLOW "%d " ANSI_RST, mem[i]);
        }
        else if (mem[i] < 0) {
            printf(ANSI_RED "%d " ANSI_RST, mem[i]);
        }
        else
            printf("%d ", mem[i]);
    }
    printf("\n\n");
}


void debug_print_list(void *memory_block) {
    struct header *current_free = memory_block;

    printf(ANSI_GREEN "Overview List of free blocks:" ANSI_RST "\n");

    int current_offset = 0;
    printf("Current Offset: %d\n", current_offset);

    while (0 != current_free->offset_of_next) {
        printf("Next Free at Offset: %d\n", current_free->offset_of_next);
        current_free = get_header(current_free->offset_of_next);
    }
    current_offset = get_offset(current_free);
    printf("Current Offset: %d (Last free)\n\n", current_offset);
}

/* ------------ Utilities ------------ */

struct header *get_header(unsigned int offset) {
    return (struct header *) ((BYTE *) MEMORY + offset);
}


int get_offset(struct header *desired_block) {
    return (BYTE *) desired_block - (BYTE *) MEMORY;
}


void merge_free_blocks(unsigned int offset_left, unsigned int offset_right) {
    /* Update list of free block, Merge size of both blocks */
    struct header *left_block = get_header(offset_left);
    struct header *right_block = get_header(offset_right);

    left_block->offset_of_next = right_block->offset_of_next;
    left_block->size += right_block->size;
//    printf(ANSI_YELLOW "[log]" ANSI_RST  " Blocks merged\n");
}

/* ------------ Assigment ------------ */

void memory_init(void *ptr, unsigned int size) {
    /* Debug purpose */
   memset(ptr, 0, size);

    /* Assign ptr as MEMORY to be work with */
    MEMORY = ptr;

    /* Init MEMORY header */
    struct header *memory_header = get_header(0);
    memory_header->offset_of_next = HEADER_SIZE;
    memory_header->size = size - HEADER_SIZE;

    /* Init First Free Block in memory */
    struct header *free_block = get_header(memory_header->offset_of_next);
    /* Offset of next free block = 0 (non exists) */
    free_block->offset_of_next = 0;
    free_block->size = size - HEADER_SIZE;
}


int memory_check(void *ptr) {
    /* If ptr is MEMORY itself */
    if (ptr == MEMORY) return 0;

    struct header *memory_header = MEMORY;
    struct header *block_to_check = ptr;

    /* If ptr is in range of MEMORY */
    if (ptr > MEMORY && ptr < (MEMORY + memory_header->size))
        /* If offset of block is valid */
        if (block_to_check->offset_of_next < memory_header->size)
            /* If size of partition is negative <=> Allocated */
            if (block_to_check->size < 0) return 1;

    return 0;
}


void *memory_alloc(unsigned int size) {
    struct header *memory_header = MEMORY;

    /* If no free partition available (full memory) */
    if (memory_header->offset_of_next == 0) {
//        printf(ANSI_RED "[MALLOC]" ANSI_RST  " No free partition available!\n");
        return NULL;
    }


    /* If size is odd number */
    if (size % 2 != 0) size += 1;

    unsigned int requested_size = size + HEADER_SIZE;

    /* If desired size is bigger than memory_block itself */
    if (memory_header->size <= requested_size) {
//        printf(ANSI_RED "[MALLOC]" ANSI_RST " No such resources available!\n");
        return NULL;
    }

    boolean block_found = FALSE;
    struct header *previous_block = memory_header;
    struct header *current_block = memory_header;

    /* Roll through list of free block */
    do {
        /* Get next block */
        previous_block = current_block;
        current_block = get_header(current_block->offset_of_next);

        /* If current block meets specification */
        if (current_block->size >= requested_size){
            block_found = TRUE;
            break;
        }

    } while (current_block->offset_of_next != 0);

    /* Return NULL if no free block meets requested size */
    if (block_found == FALSE) return NULL;

    /* If it's possible to create even new "free header" inside same block */
    if (current_block->size >= requested_size + SMALLEST_BLOCK) {
        /* Init next free block, just behind current */
        unsigned int next_offset = previous_block->offset_of_next + requested_size;
        struct header *next_free = get_header(next_offset);

        /* Update list, let next points to current's next */
        next_free->offset_of_next = current_block->offset_of_next;
        next_free->size = current_block->size - requested_size;

        /* Update list, link previous with next to remove current from list */
        previous_block->offset_of_next += requested_size;

        /* [ MALLOC ] Set block as allocated (negative size) */
        current_block->size = size * (-1);

        /* Offset set to 2 (debug print purpose) */
        current_block->offset_of_next = 2;
        /* Debug and print purpose, allocated cells set to 5 */
        int cnt_offset = get_offset(current_block);
        memset(((BYTE *) MEMORY + cnt_offset + HEADER_SIZE), 5, size);

    }
    /* Not possible to create another free block, just update list */
    else {
        /* Determine if any hanging bytes will be present after malloc */
        int no_hanging_bytes = current_block->size - requested_size;

        /* Update list, let previous points to current's offset-of-next */
        previous_block->offset_of_next = current_block->offset_of_next;

        /* [ MALLOC ] Set block as allocated (negative size) */
        current_block->size = size * (-1);

        /* Offset set to 2, Allocated cells set to 5 (debug print purpose) */
        current_block->offset_of_next = 2;
        int offset_current = get_offset(current_block);
        memset(((BYTE *) MEMORY + offset_current + HEADER_SIZE), 5, size);

        /* Hanging bytes set to 3 (debug print purpose) */
        if (no_hanging_bytes > 0)
            memset(((BYTE *) MEMORY + offset_current + HEADER_SIZE + size), 3, no_hanging_bytes);
    }

    return (BYTE *) current_block;
}


int memory_free(void *valid_ptr) {
    /* STDERR Check */
    if (valid_ptr == NULL) {
        printf(ANSI_YELLOW "[log]" ANSI_RST  " Not valid block to free (null-ptr)\n");
        return 1;
    }

    struct header *memory_header = MEMORY;

    struct header *block_to_free = valid_ptr;
    int offset_block_to_free = get_offset(block_to_free);

    /* As this assign it's first free */
    struct header *nearby_free = memory_header;

    /* Check if block is free or allocated */
    if (block_to_free->size > 0) {
        printf(ANSI_YELLOW "[log]" ANSI_RST  " Not possible to free not allocated block\n");
        return 1;
    }

    /* [ FREE ] Set block as free (positive size)  */
    block_to_free->size *= (-1);

    /* Traverse through list of free block */
    while (nearby_free->offset_of_next < offset_block_to_free) {
        if (nearby_free->offset_of_next == 0) break;

        nearby_free = get_header(nearby_free->offset_of_next);
    }
    int offset_nearby_free = get_offset(nearby_free);

    /* There is no free block */
    if (nearby_free == memory_header) {
        /* || MEM_HEAD | TO_FREE | ALLOCATED | ... */
        block_to_free->offset_of_next = memory_header->offset_of_next;
        memory_header->offset_of_next = offset_block_to_free;

        /* || MEM_HEAD | TO_FREE | FREE | ... */
        if (block_to_free->offset_of_next == offset_block_to_free + HEADER_SIZE + block_to_free->size) {
            /* Merge two free blocks next to each other */
            merge_free_blocks(offset_block_to_free, block_to_free->offset_of_next);
        }

        /*
         * Hanging bytes at the end of memory:
         *    ... | ALLOCATED | TO_FREE | NOT_USED_BYTES ||
         *    This happens only when last few bytes ( =< HEADER_SIZE))
         *    are left hanging at the end of memory block.
         * */
        if (offset_block_to_free + block_to_free->size >= memory_header->size - HEADER_SIZE) {
            block_to_free->size = memory_header->size - offset_block_to_free;
        }
        /* Debug and print purpose, freed cells set to 0 */
        memset(((BYTE *) MEMORY + offset_block_to_free + HEADER_SIZE), 0, block_to_free->size);
    }

    /* ... | FREE | TO_FREE | ?????? | ... */
    else if (offset_nearby_free + nearby_free->size == offset_block_to_free) {
        /* ... | FREE | TO_FREE | ALLOCATED | ... */
        /* Update size of merged blocks (offset_of_next stays same) */
        nearby_free->size += block_to_free->size;

        /* ... | FREE + (merged with) + TO_FREE | FREE | ... */
        if (offset_nearby_free + HEADER_SIZE + nearby_free->size == nearby_free->offset_of_next) {
            /* Merge two free blocks next to each other */
            merge_free_blocks(offset_nearby_free, nearby_free->offset_of_next);
        }

        /* Debug and print purpose, freed cells set to 0 */
        offset_block_to_free = offset_nearby_free;
        block_to_free = nearby_free;
        memset(((BYTE *) MEMORY + offset_block_to_free + HEADER_SIZE), 0, block_to_free->size);
    }

    /* ... | ALLOCATED | TO_FREE | ?????? | ... */
    else {
        /* ... | ALLOCATED | TO_FREE | ALLOCATED | ... */
        block_to_free->offset_of_next = nearby_free->offset_of_next;
        nearby_free->offset_of_next = offset_block_to_free;

        /* ... | ALLOCATED | TO_FREE | FREE | ... */
        if (block_to_free->offset_of_next == offset_block_to_free + HEADER_SIZE + block_to_free->size){
            unsigned int offset_right_free = offset_block_to_free + HEADER_SIZE + block_to_free->size;
            merge_free_blocks(offset_block_to_free, offset_right_free);
        }

        /* Debug and print purpose, freed cells set to 0 */
        memset(((BYTE *) MEMORY + offset_block_to_free + HEADER_SIZE), 0, block_to_free->size);
    }

    /* -----|| Hanging bytes LEFT check ||----- */
    if (offset_block_to_free - HEADER_SIZE  >= 0) {
        /* Pointer just in front of current block */
        BYTE *cell = (MEMORY + offset_block_to_free) - 1;
        /* No. of hanging bytes is limited to <= HEADER_SIZE  */
        short no_hanging_bytes = 0;

        /* Determine no. of hanging cells (indicated by 3) */
        for (int counter = 0; counter < HEADER_SIZE; counter++) {
            if (*cell == 3) no_hanging_bytes += 1;

            /* Determine if it's not inside memory_header */
            if (cell - ((BYTE *) MEMORY + HEADER_SIZE) <= 0) break;

            /* Go to left cell */
            cell -= 1;
        }
        /* If any found add to current block */
        if (no_hanging_bytes > 0) {
            /* On start position of hanging bytes we create new header */
            struct header *new_free = get_header(offset_block_to_free - no_hanging_bytes);
            /* Offset stays same */
            new_free->offset_of_next = block_to_free->offset_of_next;
            /* Expand size about No. of hanging bytes */
            new_free->size = block_to_free->size + no_hanging_bytes;

            block_to_free = new_free;
            offset_block_to_free = get_offset(block_to_free);

            /* Update offset in list (closest or memory header) */
            if (block_to_free->offset_of_next == 0) {
                nearby_free->offset_of_next = offset_block_to_free;
            }

            /* Debug and print purpose, freed cells set to 0 */
            memset(((BYTE *) MEMORY + offset_block_to_free + HEADER_SIZE), 0, block_to_free->size);
        }
    }
    /* -----|| Hanging bytes RIGHT check ||----- */
    if (offset_block_to_free + block_to_free->size < memory_header->size + HEADER_SIZE) {
        /* Pointer just behind current block */
        BYTE *cell = MEMORY + offset_block_to_free + HEADER_SIZE + block_to_free->size;
        /* No. of hanging bytes is limited to <= HEADER_SIZE  */
        short no_hanging_bytes = 0;

        /* Determine no. of hanging cells (indicated by 3) */
        for (int counter = 0; counter < HEADER_SIZE; counter++) {
            if (*cell == 3) no_hanging_bytes += 1;

            /* Go to right cell */
            cell += 1;
        }
        /* If any found add to current block */
        if (no_hanging_bytes > 0) {
            /* Expand size of current block by no of hanging bytes */
            block_to_free->size += no_hanging_bytes;
            /* Debug and print purpose, freed cells set to 0 */
            memset(((BYTE *) MEMORY + offset_block_to_free + HEADER_SIZE), 0, block_to_free->size);
        }
    }

    /* Add HEADER_SIZE (required by implementation of memory_alloc) */
    block_to_free->size += HEADER_SIZE;
    return 0;
}