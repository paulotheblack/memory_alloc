/*
 * @author: Michal Paulovic (xpaulovicm1)
 *
 * @scenario 3:
 *      Zaplnenie pamate, uvolnenie 1 bloku a realokacia,
 *      pamat by po tomto teste mala byt znova plna.
 *          memory_header->offset_of_next == 0  (0.B)
 *
 * Expected results:
 *      Malloc Requests: 5
 *      Malloc Correct: 5
 *      Success ratio: 100.00%
 *
 *      Free Requests: 1
 *      Free Correct: 1
 *      Success ratio: 100.00%
 * */

#include <stdio.h>
#include "../src/memalloc.h"

#define TEST_SIZE 132

int main() {
    char region[TEST_SIZE];

    float malloc_requests = 0;
    float malloc_correct = 0;
    float malloc_ratio = 0;

    float free_requests = 0;
    float free_correct = 0;
    float free_ratio = 0;

    char *a = NULL;
    char *b = NULL;
    char *c = NULL;
    char *d = NULL;

    memory_init(region, TEST_SIZE);

    malloc_requests += 1;
    if ((a = memory_alloc(28)) != NULL) malloc_correct += 1;

    malloc_requests += 1;
    if ((b = memory_alloc(28)) != NULL) malloc_correct += 1;

    malloc_requests += 1;
    if ((c = memory_alloc(28)) != NULL) malloc_correct += 1;

    malloc_requests += 1;
    if ((d = memory_alloc(28)) != NULL) malloc_correct += 1;
    debug_print(region, TEST_SIZE);

    free_requests += 1;
    if (b != NULL) {
        if (memory_free(b) == 0) free_correct += 1;
    }
    debug_print(region, TEST_SIZE);

    if (free_correct == 1) {
        malloc_requests += 1;
        if (memory_alloc(28) != NULL) malloc_correct += 1;
    }

    debug_print(region, TEST_SIZE);

    malloc_ratio = (malloc_correct / malloc_requests) * 100;
    free_ratio = (free_correct / free_requests) * 100;

    printf("\t----------" ANSI_RED "|[RESULT]|" ANSI_RST "----------\n");
    printf("\t\tMalloc Requests: %.0f\n", malloc_requests);
    printf("\t\tMalloc Correct: %.0f\n", malloc_correct);
    printf("\t\tSuccess ratio: %.2f%%\n", malloc_ratio);

    printf("\n\t\tFree Requests: %.0f\n", free_requests);
    printf("\t\tFree Correct: %.0f\n", free_correct);
    printf("\t\tSuccess ratio: %.2f%%\n", free_ratio);

    return 0;
}
