/*
 * @author: Michal Paulovic (xpaulovicm1)
 *
 * @scenario 1:
 *      Zaplnenie pamate celej pamate a jedna poziadavka naviac.
 *      Rovnake bloky velkosti 12B (+4B HEADER)
 *
 *      1. Vykreslenie pamate po inicializacii
 *      2. Alokovanie 8x12B
 *      3. Vykreslenie pamate
 *      4. Poziadavka na dalsich 12B (ma byt odmietnuta)
 *
 * Expected results:
 *      Malloc Requests: 9
 *      Malloc Correct: 9
 *      Success ratio: 100.00%
 *
 * */

#include <stdio.h>
#include "../src/memalloc.h"

#define TEST_SIZE 132

int main() {
    char region[TEST_SIZE];

    float malloc_requests = 0;
    float malloc_correct = 0;
    float ratio = 0;

    memory_init(region, TEST_SIZE);
    debug_print(region, TEST_SIZE);

    for (int i = 0; i < 8; i++) {
        malloc_requests += 1;
        if (memory_alloc(12) != NULL) malloc_correct += 1;
    }

    debug_print(region, TEST_SIZE);

    malloc_requests += 1;
    /* No more space in memory to allocate, return should be NULL */
    if (memory_alloc(12) == NULL) malloc_correct += 1;

    ratio = (malloc_correct / malloc_requests) * 100;

    printf("\t----------" ANSI_RED "|[RESULT]|" ANSI_RST "----------\n");
    printf("\t\tMalloc Requests: %.0f\n", malloc_requests);
    printf("\t\tMalloc Correct: %.0f\n", malloc_correct);
    printf("\t\tSuccess ratio: %.2f%%\n", ratio);

    return 0;
}