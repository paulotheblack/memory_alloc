/*
 * @author: Michal Paulovic (xpaulovicm1)
 *
 * @scenario 10:
 *      Nahodne velkosti blokov v nahodne velkych regionoch regionoch
 *
 * Results provided on stdout;
 * */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "../src/memalloc.h"

void swap(int *xp, int *yp) {
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void random_tester(char *memory, char **ptr, int min_memory, int max_memory, int min_block, int max_block) {
    int success_bytes = 0;
    int success_count = 0;

    int request_bytes = 0;
    int request_count = 0;

    int invalid_requests = 0;

    unsigned int i = 0;

    int rnd_memory = (rand() % (max_memory - min_memory + 1)) + min_memory;

    memset(memory, 0, 65535);
    memory_init(memory, rnd_memory);

    while (request_bytes <= rnd_memory - min_block) {
        int rnd = (rand() % (max_block - min_block + 1)) + min_block;

        if (request_bytes + rnd > rnd_memory) {
            invalid_requests += 1;
            continue;
        }

        request_bytes += rnd;
        request_count += 1;

        ptr[i] = (char *) memory_alloc(rnd);
        if (ptr[i]) {
            success_bytes += rnd;
            success_count += 1;
            i += 1;
        }
    }

    float result_count = ((float) success_count / (float) request_count) * 100;
    float result_bytes = ((float) success_bytes / (float) request_bytes) * 100;

    printf("\t--------------" ANSI_BLUE "[CONFIG]" ANSI_RST "--------------\n");
    printf("\t\t\tMEMORY_SIZE = %dB \n", rnd_memory);
    printf("\tMIN_BLOCK = %dB , MAX_BLOCK = %dB\n", min_block, max_block);
    printf("\t--------------" ANSI_RED "[RESULT]" ANSI_RST "--------------\n");
    printf("\tEfficiency: %.2f%% Blocks [%.2f%% B]\n", result_count, result_bytes);
    printf("\tRequests: %d Correct, %d Invalid\n\n", request_count, invalid_requests);

}

int main () {
    char region[65535];
    char *ptr[10000];
    srand(time(NULL));

    random_tester(region, ptr, 50, 100, 8, 24);
    random_tester(region, ptr, 100, 200, 8, 24);
    random_tester(region, ptr, 200, 500, 8, 24);

    printf(ANSI_GREEN "# ---------- ULTIMATE RND TEST ----------- #" ANSI_RST "\n");

    for (int i = 0; i < 100; i++) {
        random_tester(region, ptr, 1000, 65535, 1, 1000000);
    }

    return 0;
}
