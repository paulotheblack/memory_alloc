/*
 * @author: Michal Paulovic (xpaulovicm1)
 *
 * @scenario 8: Hanging bytes LEFT and RIGHT
 *      Alokujeme cely blok pamate, pomocou alokacie a uvolnovania
 *      vytvorime 'Hanging bytes' vedla alokovanych blokov. Budeme
 *      testovat, ci pri naslednom uvolnovani najblizsich blokov k
 *      nim budu pripojene
 *
 * Expected results:
 *      Free Requests: 3
 *      Free Merge: 3
 *      Success ratio: 100.00%
 * */

#include <stdio.h>
#include "../src/memalloc.h"

#define TEST_SIZE 132

int main() {
    char region[TEST_SIZE];

    float free_requests = 0;
    float free_correct = 0;
    float free_ratio = 0;

    char *a = NULL;
    char *b = NULL;
    char *c = NULL;

    memory_init(region, TEST_SIZE);

    a = memory_alloc(12);
    b = memory_alloc(12);
    c = memory_alloc(12);


    for (int i = 0; i < 5; i++) {
        memory_alloc(12);
    }

    if (b != NULL) {
        free_requests += 1;
        if (memory_free(b) == 0) free_correct += 1;
    }
    debug_print(region, TEST_SIZE);

    b = memory_alloc(10);
    debug_print(region, TEST_SIZE);


    if (c != NULL) {
        free_requests += 1;
        memory_free(c);
    }
    debug_print(region, TEST_SIZE);


    struct header *last_header = get_header(34);
    if (last_header->size == 18) free_correct += 1;

    b = memory_alloc(12);
    debug_print(region, TEST_SIZE);

    if (b != NULL) {
        free_requests += 1;
        memory_free(b);
    }
    last_header = get_header(34);
    if (last_header->size == 18) free_correct += 1;

    debug_print(region, TEST_SIZE);

    free_ratio = (free_correct / free_requests) * 100;

    printf("\t----------" ANSI_RED "|[RESULT]|" ANSI_RST "----------\n");
    printf("\t\tFree Requests: %.0f\n", free_requests);
    printf("\t\tFree Merge: %.0f\n", free_correct);
    printf("\t\tSuccess ratio: %.2f%%\n", free_ratio);

    return 0;
}