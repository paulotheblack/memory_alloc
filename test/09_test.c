/*
 * @author: Michal Paulovic (xpaulovicm1)
 *
 * @scenario 9:
 *      Kontrola spravneho fungovania memory check
 *
 * Expected results:
 *      Check Requests: 5
 *      Check Correct : 5
 *      Success ratio: 100.00%
 * */

#include <stdio.h>
#include "../src/memalloc.h"

#define TEST_SIZE 132

int main() {
    char region[TEST_SIZE];

    float check_request = 0;
    float check_correct = 0;
    float check_ratio = 0;

    char *a = NULL;
    char *b = NULL;
    char *c = NULL;

    memory_init(region, TEST_SIZE);

    for (int i = 0; i < 2; i++) {
        memory_alloc(12);
    }

    a = memory_alloc(12);
    b = memory_alloc(12);
    c = memory_alloc(12);


    for (int i = 0; i < 3; i++) {
        memory_alloc(12);
    }

    if (a != NULL) {
        check_request += 1;
        if (memory_check(a) == 1) check_correct += 1;
    }

    if (b != NULL) {
        check_request += 1;
        if (memory_check(a) == 1) check_correct += 1;
    }

    if (memory_free(b) == 0) {
        check_request += 1;
        if (memory_check(b) == 0) check_correct += 1;
    }

    debug_print(region, TEST_SIZE);

    if (c != NULL) {
        check_request += 1;
        if (memory_check(c) == 1) check_correct += 1;
    }

    if (memory_free(c) == 0) {
        check_request += 1;
        if (memory_check(c) == 0) check_correct += 1;
    }

    debug_print(region, TEST_SIZE);


    check_ratio = (check_correct / check_request) * 100;

    printf("\t----------" ANSI_RED "|[RESULT]|" ANSI_RST "----------\n");
    printf("\t\tCheck Requests: %.0f\n", check_request);
    printf("\t\tCheck Correct: %.0f\n", check_correct);
    printf("\t\tSuccess ratio: %.2f%%\n", check_ratio);

    return 0;
}

