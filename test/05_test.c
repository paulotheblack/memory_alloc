/*
 * @author: Michal Paulovic (xpaulovicm1)
 *
 * @scenario 5:
 *      Zaplnenie celej pamate, na zaciatku a na konci uvolnit bloky pamate.
 *      Nasledne uvolnit bloky co su im najblizsie (Merge zprava a zlava).
 *      Takisto sledujeme, ci pri tychto operaciach neprekrocime hranice
 *      bloku celkovej pamate.
 *
 *      1. Zaplnenie celej pamate po blokoch velkosti 28B (+4B HEADER)
 *      2. Uvolnenie bloku na offsete 4
 *          MEM_HEADER->offset_of_next == 4     (0.B)
 *          FIRST_FREE->offset_of_next == 0     (4.B)
 *          FIRST_FREE->size == 32              (6.B)
 *
 *      3. Uvolnenie bloku na offsete 228
 *          MEM_HEADER->offset_of_next == 4     (0.B)
 *          FIRST_FREE->offset_of_next == 228   (4.B)
 *          NEXT_FREE->offset_of_next == 0      (228.B)
 *          NEXT_FREE->size == 32               (230.B)
 *
 *      4. Uvolnenie bloku na offsete 36 (Merge zlava)
 *          MEM_HEADER->offset_of_next == 4     (0.B)
 *          FIRST_FREE->offset_of_next == 228   (4.B)
 *          FIRST_FREE->size == 64              (6.B)
 *          NEXT_FREE->offset_of_next == 0      (228.B)
 *          NEXT_FREE->size == 32               (230.B)
 *
 *      5. Uvolnenie bloku na offsete 196 (Merge zlava)
 *          MEM_HEADER->offset_of_next == 4     (0.B)
 *          FIRST_FREE->offset_of_next == 196   (4.B)
 *          FIRST_FREE->size == 64              (6.B)
 *          NEXT_FREE->offset_of_next == 0      (196.B)
 *          NEXT_FREE->size == 64               (198.B)
 *
 *
 *
 * Expected results:
 *      Malloc Requests: 8
 *      Malloc Correct: 8
 *      Success ratio: 100.00%
 *
 *      Free Requests: 4
 *      Free Correct: 4
 *      Success ratio: 100.00%
 * */

#include <stdio.h>
#include "../src/memalloc.h"

#define TEST_SIZE 260

int main() {
    char region[TEST_SIZE];

    float malloc_requests = 0;
    float malloc_correct = 0;
    float malloc_ratio = 0;

    float free_requests = 0;
    float free_correct = 0;
    float free_ratio = 0;

    char *a = NULL;
    char *b = NULL;
    char *c = NULL;
    char *d = NULL;

    memory_init(region, TEST_SIZE);

    malloc_requests += 1;
    if ((a = memory_alloc(28)) != NULL) malloc_correct += 1;

    malloc_requests += 1;
    if ((b = memory_alloc(28)) != NULL) malloc_correct += 1;

    for (int i = 0; i < 4; i++) {
        malloc_requests += 1;
        if (memory_alloc(28) != NULL) malloc_correct += 1;
    }

    malloc_requests += 1;
    if ((c = memory_alloc(28)) != NULL) malloc_correct += 1;

    malloc_requests += 1;
    if ((d = memory_alloc(28)) != NULL) malloc_correct += 1;

    debug_print(region, TEST_SIZE);

    if (a != NULL) {
        free_requests += 1;
        if (memory_free(a) == 0) free_correct += 1;
    }
    debug_print(region, TEST_SIZE);
    debug_print_list(region);

    if (d != NULL) {
        free_requests += 1;
        if (memory_free(d) == 0) free_correct += 1;
    }
    debug_print(region, TEST_SIZE);
    debug_print_list(region);

    if (b != NULL) {
        free_requests += 1;
        if (memory_free(b) == 0) free_correct += 1;
    }
    debug_print(region, TEST_SIZE);
    debug_print_list(region);

    if (c != NULL) {
        free_requests += 1;
        if (memory_free(c) == 0) free_correct += 1;
    }
    debug_print(region, TEST_SIZE);
    debug_print_list(region);

    malloc_ratio = (malloc_correct / malloc_requests) * 100;
    free_ratio = (free_correct / free_requests) * 100;

    printf("\t----------" ANSI_RED "|[RESULT]|" ANSI_RST "----------\n");
    printf("\t\tMalloc Requests: %.0f\n", malloc_requests);
    printf("\t\tMalloc Correct: %.0f\n", malloc_correct);
    printf("\t\tSuccess ratio: %.2f%%\n", malloc_ratio);

    printf("\n\t\tFree Requests: %.0f\n", free_requests);
    printf("\t\tFree Correct: %.0f\n", free_correct);
    printf("\t\tSuccess ratio: %.2f%%\n", free_ratio);

    return 0;
}