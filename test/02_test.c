/*
 * @author: Michal Paulovic (xpaulovicm1)
 *
 * @scenario 2:
 *      Poziadavka na alokaciu vacsej pamate ako je mozne.
 *      REQUESTED_SIZE > TEST_SIZE
 *
 * Expected results:
 *      Malloc Requests: 4
 *      Malloc Correct: 4
 *      Success ratio: 100.00%
 * */

#include <stdio.h>
#include "../src/memalloc.h"

#define TEST_SIZE 132

int main() {
    char region[TEST_SIZE];

    float malloc_requests = 0;
    float malloc_correct = 0;
    float ratio = 0;

    memory_init(region, TEST_SIZE);

    malloc_requests += 1;
    if (memory_alloc(150) == NULL) malloc_correct += 1;

    malloc_requests += 1;
    if (memory_alloc(1000) == NULL) malloc_correct += 1;

    malloc_requests += 1;
    if (memory_alloc(666) == NULL) malloc_correct += 1;

    malloc_requests += 1;
    if (memory_alloc(132) == NULL) malloc_correct += 1;

    ratio = (malloc_correct / malloc_requests) * 100;

    printf("\t----------" ANSI_RED "|[RESULT]|" ANSI_RST "----------\n");
    printf("\t\tMalloc Requests: %.0f\n", malloc_requests);
    printf("\t\tMalloc Correct: %.0f\n", malloc_correct);
    printf("\t\tSuccess ratio: %.2f%%\n", ratio);

    return 0;
}